---
layout: job_family_page
title: "Analysts Relations Manager"
---

## Analysts Relations Manager

As our Global Industry Analysts and Influencers Relations Manager, you will manage all analyst, influencers, and thought leadership relationships for GitLab. 

### Responsibilities

- Manage and execute the overall industry analysts, influencers and thought leaders strategy and plan for GitLab. 
- Manage, maintain, and be the central point for positive relationships with industry analysts, industry influencers, and thought leaders.
- Establish and maintain regular communication with analysts and influencers via myriad channels which you will develop to engage and educate these communities on GitLab strategy, roadmap and product updates.
- Ensure all analysts and influencer advice informs the business and reports mentioning GitLab are leveraged for key marketing awareness and lead generation campaigns.
- Plan, manage and schedule influencer program activities such as participation in analyst conferences, advisory days, briefings, inquiries, and reporting.
- Drive alignment with sales, marketing and product teams to maximize program impact.
- Schedule briefings, inquiries, demos and advisory days with key analysts and influencers, GitLab customers and GitLab executives.
- Prioritize and manage incoming analyst requests for information, research reports, research review, customer references and event speakers, and research.
- Secure, manage and ensure contracts and related services are strategic, meet company needs now and into the future and are fully utilized.
- Serve as Program Manager for GitLab Customer Advisory Boards and GitLab Thought Leadership Advisory Board.


### Requirements

- Prior experience working with analysts and influencers in B2B enterprise software and services required.
- Understanding of how to build relationships with key influencers and leading industry analysts.
- Excellent project and time management skills.
- Excellent written and oral communications skills.
- Self-starter with a strong sense of ownership.
- Able to prioritize in a complex, fast-paced and lean organization.
- Passion for building a world class analyst program and desire to own and refine key operational processes 
- You share our [values](/handbook/values/), and work in accordance with those values

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with a Senior Product Marketing Manager
- Candidates will then be invited to schedule 45 minute interviews with our Regional Director of Sales-West.
- Then, Candidates will be invited to schedule 45 minute interviews with Director of Product Marketing.
- Candidates will also be invited to schedule 45 minute interviews with our Director of Customer Success.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
