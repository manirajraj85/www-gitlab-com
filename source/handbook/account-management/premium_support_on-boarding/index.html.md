---
layout: markdown_page
title: "On-boarding process for Premium Support"
---
The contents of this page have been moved to the [Customer Success Handbook](/handbook/customer-success/account-management/premium_support_on-boarding)